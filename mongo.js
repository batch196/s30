/*
db.fruits.insertMany([
{
    "_id" : ObjectId("62e1dd635db01896603075d1"),
    "name" : "Apple",
    "supplier" : "Red Farms Inc.",
    "stocks" : "20",
    "price" : "40",
    "onSale" : true
},


{
    "_id" : ObjectId("62e1dd635db01896603075d2"),
    "name" : "Banana",
    "supplier" : "Yellow Farms",
    "stocks" : "15",
    "price" : "20",
    "onSale" : true
},


{
    "_id" : ObjectId("62e1dd635db01896603075d3"),
    "name" : "Kiwi",
    "supplier" : "Green Farming and Canning",
    "stocks" : "25",
    "price" : "50",
    "onSale" : true
},


{
    "_id" : ObjectId("62e1dd635db01896603075d4"),
    "name" : "Mango",
    "supplier" : "Yellow Farms",
    "stocks" : "10",
    "price" : "60",
    "onSale" : true
},


{
    "_id" : ObjectId("62e1dd635db01896603075d5"),
    "name" : "Dragon Fruit",
    "supplier" : "Red Farms Inc.",
    "stocks" : "10",
    "price" : "60",
    "onSale" : true
}
])
*/


db.fruits.aggregate([

    {$match: {onSale:true}},
    {$group: {_id:"$supplier",totalStocks:{$sum:"$stocks"}}}
    
]) //to fix $sum not working

db.fruits.aggregate([

    {$match: {onSale:true}},
    {$group: {_id:null,totalStocks:{$sum:"$stocks"}}}
    
])
    
db.fruits.aggregate([

    {$match: {onSale:true}},
    {$group: {_id:"AllFruits",totalStocks:{$sum:"$stocks"}}}
    
]) 
db.fruits.aggregate([

    {$match: {onSale:true}},
    {$group: {_id:"$supplier",avgStock:{$avg:"$stocks"}}}
    
]) 
db.fruits.aggregate(
    [
        {$match: {onSale:true}},
        {$group: {_id:null,averagePrice: {$avg: "$price"}}}
     ]
)


//($max) -maximum

db.fruits.aggregate(
    [
        {$match:{onSale:true}},
        {$group:{_id:"highestStockOnSale",maxStock: {$max: "$stocks"}}}

    ]
)
        
db.fruits.aggregate(
    [
        {$match:{onSale:true}},
        {$group:{_id:null,maxPrice: {$max: "$price"}}}

    ]
)

db.fruits.aggregate(
       [
            {$match:{onSale:true}},
            {$group:{_id:"lowestStock",minStock:{$min:"$stocks"}}}
       
       ]
 )
db.fruits.aggregate(
       [
            {$match:{onSale:true}},
            {$group:{_id:"lowestPrice",minPrice:{$min:"$price"}}}
       
       ]
 )
db.fruits.aggregate([
        {$match:{onSale:true}},
        {$count: "itemsOnSale"}
    ])
        











