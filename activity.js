db.fruits.aggregate([

        {$match:{supplier:"Yellow Farms"}},
        {$match:{price:{$lt:50}}},
        {$count: "lessthan50"}
        
 ])
db.fruits.aggregate([
        
        {$match:{price:{$lt: 50}}},
        {$count: "allFruitsLessThan50"}
])
db.fruits.aggregate([
        
        {$match:{supplier:"Yellow Farms"}},
        {$group:{_id:"averagePrice",AveragePrices: {$avg: "$price"}}}
])
db.fruits.aggregate([
        
        {$match:{supplier:"Red Farms Inc."}},
        {$group:{_id:"ahighest",highestPrice: {$max: "$price"}}}
])
db.fruits.aggregate([
        
        {$match:{supplier:"Red Farms Inc."}},
        {$group:{_id:"lowest",lowestPrice: {$min: "$price"}}}
])